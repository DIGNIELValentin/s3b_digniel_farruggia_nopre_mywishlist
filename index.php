<?php


ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once 'vendor/autoload.php';


use \mywishlist\models\Item as Item;
use \mywishlist\models\Liste as Liste;
use \mywishlist\controllers\ControllerAccueil;
use \mywishlist\controllers\ControllerParticipant;
use \mywishlist\controllers\ControllerGet;
use \mywishlist\controllers\ControllerPost;
use \mywishlist\controllers\ControllerItem;
use \mywishlist\controllers\ControllerListe;
use \Illuminate\Database\Capsule\Manager as DB;


$db = new DB();
$db->addConnection( [
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'database' => 'php',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim;

$app->get('/', function (){
    ControllerAccueil::afficherAccueil();
});

$app->get('/GET', function (){
    ControllerGet::afficherAccueil();
});

$app->get('/GET/LISTES', function (){
    ControllerParticipant::afficher(1, Liste::toutesLesListes());
});

$app->get('/GET/LISTE', function (){
    ControllerGet::afficherChoixListe();
});

$app->get('/GET/LISTE/:no', function ($no){
    ControllerParticipant::afficher(4, Liste::chercherListe($no));
});

$app->get('/GET/ITEMS', function (){
    ControllerParticipant::afficherChoix();
    //ControllerParticipant::afficher(2, Liste::toutesLesListes(), Item::tousLesItems());
});

$app->get('/GET/ITEMS/:id', function ($id){
    ControllerParticipant::afficher(2, Liste::chercherListe($id), Item::tousLesItems());
});

$app->get('/GET/ITEM', function (){
    ControllerGet::afficherChoixItem();
});

$app->get('/GET/ITEM/:item', function ($item){
    ControllerParticipant::afficher(3, null, Item::chercherUnItem($item));
});

$app->get('/POST', function (){
    ControllerPost::afficherAccueil();
});

$app->post('/POST/LISTE', function (){
    $ctrl = new ControllerListe();
    $ctrl->creerListe();
    ControllerListe::afficherReussi();
});

$app->post('/POST/ITEM', function () {
    $ctrl = new ControllerItem();
    $ctrl->ajouterItem();
    ControllerItem::afficherReussi();
});

$app->run();
