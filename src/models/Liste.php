<?php

namespace mywishlist\models;


class Liste extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'liste';
    protected $primaryKey = 'no';

    protected $fillable = [
        'titre',
        'description',
        'expiration'
    ];

    public $timestamps = false;

    public function items() {
      return $this->hasMany('mywishlist\models\Item','liste_id','no');
    }

    public static function toutesLesListes(){
        return self::select('*')->get();
    }

    public static function chercherListeParToken($token){
        return self::where('token', '=', $token)->get();
    }

    public static function chercherListe($p_id){
        return self::where('no', '=', $p_id)->get();
    }

  }
