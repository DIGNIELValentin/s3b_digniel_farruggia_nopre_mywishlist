<?php
namespace mywishlist\controllers;

use \mywishlist\models\Item as Item;
use \mywishlist\models\Liste as Liste;
use \mywishlist\views\ViewItem as ViewItem;


class ControllerItem {

    function afficherItem($idItem) {
        $item = Item::chercherUnItem($idItem);
        $vue = new ViewItem();
        $vue->apercuItem($item);
    }

    function ajouterItem() {

        $item = new Item;

        $item->liste_id = $_POST['id'];
        $item->nom = $_POST['nom'];
        $item->descr = $_POST['descr'];
        $item->tarif = $_POST['tarif'];
        $item->save();

    }

    function createItem($list_id, $listToken) {
        $view = new ViewItem();
        $item = new Item();

        if (!isset($_POST['itemName'])) $view->erreur("veuillez entrer un nom pour l'item");
        if (!isset($_POST['itemDescription'])) $view->erreur("veuillez entrer une description");
        if (!isset($_POST['itemPrix'])) $view->erreur("Le prix est absent");
        if (!filter_var($_POST['itemPrix'], FILTER_VALIDATE_FLOAT)) $view->erreur("Votre tarif est invalide");
        if (strlen($_POST['itemName']) < 3) $view->erreur("Le nom doit avoir un minimum  de trois lettres");
        if (strlen($_POST['itemDescription']) < 10) $view->erreur("Donnez une description a l'item");


        $nom = filter_var($_POST['itemName'], FILTER_SANITIZE_STRING);
        $description = filter_var($_POST['itemDescr'], FILTER_SANITIZE_STRING);
        $prix = filter_var($_POST['itemPrix'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);


        $item->nom = $nom;
        $item->descr = $description;
        $item->tarif = $prix;
        $item->liste_id = $list_id;

        $item->save();
        $view->afficherBienEnregistrer();
    }

    public static function afficherReussi(){
        echo "<a href='../POST'>Retour</a><br>Votre ajout a bien été pris en compte";
    }

}
