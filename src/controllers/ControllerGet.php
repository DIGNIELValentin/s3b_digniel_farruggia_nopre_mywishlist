<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 16/01/2019
 * Time: 10:28
 */

namespace mywishlist\controllers;

use mywishlist\views\ViewGet;

class ControllerGet {

    public static function afficherAccueil(){
        ViewGet::afficherAccueil();
    }

    public static function afficherChoixItem(){
        ViewGet::afficherChoixItem();
    }

    public static function afficherChoixListe(){
        ViewGet::afficherChoixListe();
    }
}