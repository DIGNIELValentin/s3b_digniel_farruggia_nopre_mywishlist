<?php

namespace mywishlist\controllers;

use mywishlist\models\Liste;


class ControllerListe {

    public function creerListe(){

        if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['titre']) && isset($_POST['description']) && isset($_POST['expiration'])) {

            $liste = new Liste;
            $liste->titre = $_POST['titre'];
            $liste->description = $_POST['description'];
            $liste->expiration = $_POST['expiration'];

            $liste->save();
        }
    }

    public static function afficherReussi(){
        echo "<a href='../POST'>Retour</a><br>Votre ajout a bien été pris en compte";
    }
}
