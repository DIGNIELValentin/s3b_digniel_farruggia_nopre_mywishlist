<?php

namespace mywishlist\controllers;


use mywishlist\views\ViewParticipant;
use mywishlist\models\Liste;

class ControllerParticipant{


    public static function afficher($selecteur, $p_listes = null, $p_items = null){
        $listes = null;
        $items = null;
        if ($p_listes !== null) $listes = $p_listes;
        if ($p_items !== null) $items = $p_items;
        $vue = new ViewParticipant($items, $listes);
        $vue->afficher($selecteur);
    }

    public static function afficherChoix(){
        $res = "<a href='../GET'>Retour</a><br><br>";
        $listes = Liste::toutesLesListes();

        foreach ($listes as $liste) {
            $i = $liste->no;
            $res .= "<a href='ITEMS/$i'>Liste numéro $i</a><br>";
        }
        echo $res;
    }

}