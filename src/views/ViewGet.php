<?php

namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Liste;

class ViewGet {

    public static function afficherAccueil(){
      $res = "<html>";
      $res .= "<head>";
      $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
      $res .= "<title>GET</title>";
      $res .= "</head>";
      $res .= "<body>";
      $res .= "<header>";
      $res .= "<h1><a></a></h1>";
      $res .= "</header>";
        $res .= "<div>";
        $res .= "<a href='../index.php'>Retour</a>";
        $res .= "<h1>Vous êtes dans la section GET</h1> <br>";
        $res .= "<ul>
                    <li><a href='GET/LISTES'>/GET/LISTES</a> : pour avoir toutes les listes de la bdd</li>
                    <li><a href='GET/LISTE'>/GET/LISTE</a> : pour avoir la liste de la bdd possedant la token choisit</li>
                    <li><a href='GET/ITEMS'>/GET/ITEMS</a> : pour avoir tous les items de la bdd</li>
                    <li><a href='GET/ITEM'>/GET/ITEM</a> : pour avoir un item de la bdd</li>
                 </ul> <br>";
        $res .= "</div>";
        $res .= "</body>";
        $res .= "</html>";
        echo $res;
    }

    public static function afficherChoixItem(){
      $res = "<html>";
      $res .= "<head>";
      $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
      $res .= "<title>GET</title>";
      $res .= "</head>";
      $res .= "<body>";
      $res .= "<header>";
      $res .= "<h1><a></a></h1>";
      $res .= "</header>";
        $res .= "<a href='../GET'>Retour</a><br>Ici vous pouvez choisir des items :";
        $items = Item::tousLesItems();

        foreach ($items as $item) {
            $i = $item->id;
            $res .= "<a href='ITEM/$i'>Item numéro $i</a><br>";
        }
        $res .= "</body>";
        $res .= "</html>";
        echo $res;
    }

    public static function afficherChoixListe(){
      $res = "<html>";
      $res .= "<head>";
      $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
      $res .= "<title>GET</title>";
      $res .= "</head>";
      $res .= "<body>";
      $res .= "<header>";
      $res .= "<h1><a></a></h1>";
      $res .= "</header>";
        $res .= "<a href='../GET'>Retour</a><br>Ici vous pouvez choisir des listes :";
        $listes = Liste::toutesLesListes();

        foreach ($listes as $liste) {
            $i = $liste->no;
            $res .= "<a href='LISTE/$i'>Liste numéro $i</a><br>";
        }
        $res .= "</body>";
        $res .= "</html>";
        echo $res;
    }
}
