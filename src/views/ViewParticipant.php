<?php
namespace mywishlist\views;

use mywishlist\models\Item;
use mywishlist\models\Liste;

class ViewParticipant {

    private $listes, $items;

    public function __construct($items, $listes){
        $this->listes = $listes;
        $this->items = $items;
    }


    public function afficher($selecteur){
        switch ($selecteur){
            case 1 :
                echo self::afficherListes();
                break;
            case 2 :
                echo self::afficherItems();
                break;
            case 3 :
                echo self::afficherItem();
                break;
            case 4 :
                echo self::afficherListe();
                break;
        }
    }

    private function afficherListes(){
        $res = "<div>";
        $res .= "<a href='../GET'>Retour</a><br><br>";
        foreach ($this->listes as $liste) {
            $res .= $liste."<br>";
        }
        $res .= "</div>";
        return $res;
    }

    private function afficherItems(){
        $liste = $this->listes[0];
        $res = "<html>";
        $res .= "<head>";
        $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
        $res .= "<title>GET</title>";
        $res .= "</head>";
        $res .= "<body>";
        $res .= "<header>";
        $res .= "<h1><a></a></h1>";
        $res .= "</header>";
        $res .= "<a href='../ITEMS'>Retour</a><br><br>";

        $res .= "<div>$liste : <br>";

        $res2 = "<div><ul>";
        $this->items = $liste->items;
        foreach ($this->items as $item) {
            $res2 .= "<li>$item</li>";
        }
        $res2 .= "</ul></div>";
        $res .= "$res2";

        $res .= "</div>";
        $res .= "</body>";
        $res .= "</html>";
        return $res;
    }

    private function afficherItem(){
      $liste = $this->listes[0];
      $res = "<html>";
      $res .= "<head>";
      $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
      $res .= "<title>GET</title>";
      $res .= "</head>";
      $res .= "<body>";
      $res .= "<header>";
      $res .= "<h1><a></a></h1>";
      $res .= "</header>";
        $res .= "<div>";
        $res .= "<a href='../../GET/ITEM'>Retour</a><br><br>";
        $res .= $this->items;
        $res .= "</div>";
        $res .= "</body>";
        $res .= "</html>";
        return $res;
    }

    private function afficherListe(){
      $res = "<html>";
      $res .= "<head>";
      $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">";
      $res .= "<title>GET</title>";
      $res .= "</head>";
      $res .= "<body>";
      $res .= "<header>";
      $res .= "<h1><a></a></h1>";
      $res .= "</header>";
        $res .= "<a href='../LISTE'>Retour</a><br><br>";
        $res .= $this->listes[0];
        $res .= "</body>";
        $res .= "</html>";
        return $res;
    }



}
