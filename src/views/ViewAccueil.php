<?php
namespace mywishlist\views;

class ViewAccueil {


    public static function afficherAccueil(){
        $res = "<html>";
        $res .= "<head>";
        $res .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">";
        $res .= "<title>Accueil</title>";
        $res .= "</head>";
        $res .= "<body>";
        $res .= "<header>";
        $res .= "<h1><a></a></h1>";
        $res .= "</header>";
        $res .= "<div>";
        $res .= "<h1>Le site s'utilise comme ça :</h1> <br>";
        $res .= "<p>dans l'url on marque test.php/GET/ ensuite on choisi qu'est-ce qu'on veux<p></p> <br>";
        $res .= "<p>Ici vous avez le choix entre aller dans les deux parties <a href=\"index.php/GET\">GET</a> et <a href=\"index.php/POST\">POST</a><p></p>";
        $res .= "<ul>
                    <li>test.php/GET/ITEMS : pour avoir tous les items de la bdd</li>
                    <li>test.php/GET/ITEM : pour avoir un item de la bdd</li>
                    <li>test.php/GET/LISTES : pour avoir toutes les listes de la bdd</li>
                 </ul>";
        $res .= "<ul>
                    <li>test.php/POST : pour ajouter une liste dans la bdd</li>
                 </ul>";
        $res .= "</div>";
        $res .= "</body>";
        $res .= "</html>";
        echo $res;
    }

}
